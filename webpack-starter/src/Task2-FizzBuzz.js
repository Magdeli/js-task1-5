var fizz = "Fizz";
var buzz = "Buzz";
var i;
var arr = [];

for (i = 0; i < 50; i++) {
	if ((i + 1) % 15 === 0) {
		arr[i] = fizz + buzz;
	} else if ((i + 1) % 3 === 0) {
		arr[i] = fizz;
	} else if ((i + 1) % 5 === 0) {
		arr[i] = buzz;
	} else {
		arr[i] = " ";
	}
}

console.log(arr);
